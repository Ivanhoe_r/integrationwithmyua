import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Main {

    public static void main(String[] args) throws Exception {
        URL urltogetcookieandtoken = new URL("https://myua.my.com/oauth/mycom/");
        HttpURLConnection contogetcookieandtoken = (HttpURLConnection) urltogetcookieandtoken.openConnection();
        //Set kind of query
        contogetcookieandtoken.setRequestMethod("POST");
        //add POST request parameters to header (CURL: -H)
        contogetcookieandtoken.setRequestProperty("Content-Type", "application/json");
        contogetcookieandtoken.setRequestProperty("accept", "application/json");
        contogetcookieandtoken.addRequestProperty("Host", "myua.my.com");
        //To send params to the server
        contogetcookieandtoken.setDoOutput(true);
        //Creds for myua account
        String password = "qGmkM4MBIH", login = "it@krab.me";
        //construct JSON params
        String jsonInputString = "{\"password\": \"" + password + "\", \"login\": \"" + login + "\"}";
        System.out.println(jsonInputString);
        //To get response
        contogetcookieandtoken.setDoInput(true);
        //send params to the server using output stream
        try (OutputStream os = contogetcookieandtoken.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        //For parsing response
        int firstindexofsessionID, lastindexofsessionID, firstindexofcsrftoken, lastindexofcsrftoken;
        String stringwithcsrftoken, stringwithsessionid, sessionID, csrftoken;
        //9 - string with csrftoken
        //10 - string with cookie sessionID
        stringwithcsrftoken = contogetcookieandtoken.getHeaderField(9);
        stringwithsessionid = contogetcookieandtoken.getHeaderField(10);

        //sessionID will always be first parameter
        firstindexofsessionID = stringwithsessionid.indexOf("=");
        lastindexofsessionID = stringwithsessionid.indexOf(";");
        //Can't understand why +1
        sessionID = stringwithsessionid.substring(firstindexofsessionID + 1, lastindexofsessionID);

        //csrftoken will always be first param
        firstindexofcsrftoken = stringwithcsrftoken.indexOf("=");
        lastindexofcsrftoken = stringwithcsrftoken.indexOf(";");

        csrftoken = stringwithcsrftoken.substring(firstindexofcsrftoken + 1, lastindexofcsrftoken);

        debugprintJSONresponse(contogetcookieandtoken);
     //   debugprintallresponse(contogetcookieandtoken);

        contogetcookieandtoken.disconnect();
        //----------------------------------------------------------------------------------------------------------------
        //Connection to create folder
        URL urltocreatefolre = new URL("https://myua.my.com/api/crhub/fs/create_folder");
        HttpURLConnection contocreatefolder = (HttpURLConnection) urltocreatefolre.openConnection();
        //Set kind of query
        contocreatefolder.setRequestMethod("POST");
        //add POST request parameters to header (CURL: -H)
        contocreatefolder.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        contocreatefolder.addRequestProperty("Host", "myua.my.com");
        contocreatefolder.addRequestProperty("Origin", "https://myua.my.com");
        contocreatefolder.addRequestProperty("Referer", "https://myua.my.com");
        contocreatefolder.addRequestProperty("Cookie", "csrftoken=" + csrftoken + "; " + "sessionid=" + sessionID + ";");
        contocreatefolder.addRequestProperty("X-CSRFToken", csrftoken);
        //To send params to the server
        contocreatefolder.setDoOutput(true);
        //Parameteres for myua account
        jsonInputString = "{\"name\": \"" + "testkrab" + "\", \"parent_node_id\": \"" + "88929:1" + "\"}";
        System.out.println(jsonInputString);
        //To get response
        contocreatefolder.setDoInput(true);
        //send params to the server using output stream
        try (OutputStream os = contocreatefolder.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write( input, 0, input.length );
        }
//        debugprintallresponse( contocreatefolder );
        debugprintJSONresponse( contocreatefolder );


        contocreatefolder.disconnect();
    }
    //Debug method to print response page
    public static void debugprintallresponse(HttpURLConnection con) {
        StringBuilder builder = new StringBuilder();
        Map<String, List<String>> map = con.getHeaderFields();
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            if (entry.getKey() == null)
                continue;
            builder.append(entry.getKey())
                    .append(": ");

            List<String> headerValues = entry.getValue();
            Iterator<String> it = headerValues.iterator();
            if (it.hasNext()) {
                builder.append(it.next());

                while (it.hasNext()) {
                    builder.append(", ")
                            .append(it.next());
                }
            }

            builder.append("\n");
        }

        System.out.println(builder);
    }
    //Debug method to print JSON response
    public static void debugprintJSONresponse( HttpURLConnection con ) throws Exception
    {
            try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }
    }

}

